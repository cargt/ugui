#ifndef __UGUI_COLORS_H
#define __UGUI_COLORS_H

#include "ugui_config_types.h"


#ifdef USE_COLOR_RGB888

#define COLOR_SET_ALPHA( _color, _alpha ) ( (_color & 0x00FFFFFF) | (alpha << 24) )

#define C_TRANSPARENT 				  0x00000000

#define  C_MAROON                     0xFF800000
#define  C_DARK_RED                   0xFF8B0000
#define  C_BROWN                      0xFFA52A2A
#define  C_FIREBRICK                  0xFFB22222
#define  C_CRIMSON                    0xFFDC143C
#define  C_RED                        0xFFFF0000
#define  C_TOMATO                     0xFFFF6347
#define  C_CORAL                      0xFFFF7F50
#define  C_INDIAN_RED                 0xFFCD5C5C
#define  C_LIGHT_CORAL                0xFFF08080
#define  C_DARK_SALMON                0xFFE9967A
#define  C_SALMON                     0xFFFA8072
#define  C_LIGHT_SALMON               0xFFFFA07A
#define  C_ORANGE_RED                 0xFFFF4500
#define  C_DARK_ORANGE                0xFFFF8C00
#define  C_ORANGE                     0xFFFFA500
#define  C_GOLD                       0xFFFFD700
#define  C_DARK_GOLDEN_ROD            0xFFB8860B
#define  C_GOLDEN_ROD                 0xFFDAA520
#define  C_PALE_GOLDEN_ROD            0xFFEEE8AA
#define  C_DARK_KHAKI                 0xFFBDB76B
#define  C_KHAKI                      0xFFF0E68C
#define  C_OLIVE                      0xFF808000
#define  C_YELLOW                     0xFFFFFF00
#define  C_YELLOW_GREEN               0xFF9ACD32
#define  C_DARK_OLIVE_GREEN           0xFF556B2F
#define  C_OLIVE_DRAB                 0xFF6B8E23
#define  C_LAWN_GREEN                 0xFF7CFC00
#define  C_CHART_REUSE                0xFF7FFF00
#define  C_GREEN_YELLOW               0xFFADFF2F
#define  C_DARK_GREEN                 0xFF006400
#define  C_GREEN                      0xFF00FF00
#define  C_FOREST_GREEN               0xFF228B22
#define  C_LIME                       0xFF00FF00
#define  C_LIME_GREEN                 0xFF32CD32
#define  C_LIGHT_GREEN                0xFF90EE90
#define  C_PALE_GREEN                 0xFF98FB98
#define  C_DARK_SEA_GREEN             0xFF8FBC8F
#define  C_MEDIUM_SPRING_GREEN        0xFF00FA9A
#define  C_SPRING_GREEN               0xFF00FF7F
#define  C_SEA_GREEN                  0xFF2E8B57
#define  C_MEDIUM_AQUA_MARINE         0xFF66CDAA
#define  C_MEDIUM_SEA_GREEN           0xFF3CB371
#define  C_LIGHT_SEA_GREEN            0xFF20B2AA
#define  C_DARK_SLATE_GRAY            0xFF2F4F4F
#define  C_TEAL                       0xFF008080
#define  C_DARK_CYAN                  0xFF008B8B
#define  C_AQUA                       0xFF00FFFF
#define  C_CYAN                       0xFF00FFFF
#define  C_LIGHT_CYAN                 0xFFE0FFFF
#define  C_DARK_TURQUOISE             0xFF00CED1
#define  C_TURQUOISE                  0xFF40E0D0
#define  C_MEDIUM_TURQUOISE           0xFF48D1CC
#define  C_PALE_TURQUOISE             0xFFAFEEEE
#define  C_AQUA_MARINE                0xFF7FFFD4
#define  C_POWDER_BLUE                0xFFB0E0E6
#define  C_CADET_BLUE                 0xFF5F9EA0
#define  C_STEEL_BLUE                 0xFF4682B4
#define  C_CORN_FLOWER_BLUE           0xFF6495ED
#define  C_DEEP_SKY_BLUE              0xFF00BFFF
#define  C_DODGER_BLUE                0xFF1E90FF
#define  C_LIGHT_BLUE                 0xFFADD8E6
#define  C_SKY_BLUE                   0xFF87CEEB
#define  C_LIGHT_SKY_BLUE             0xFF87CEFA
#define  C_MIDNIGHT_BLUE              0xFF191970
#define  C_NAVY                       0xFF000080
#define  C_DARK_BLUE                  0xFF00008B
#define  C_MEDIUM_BLUE                0xFF0000CD
#define  C_BLUE                       0xFF0000FF
#define  C_ROYAL_BLUE                 0xFF4169E1
#define  C_BLUE_VIOLET                0xFF8A2BE2
#define  C_INDIGO                     0xFF4B0082
#define  C_DARK_SLATE_BLUE            0xFF483D8B
#define  C_SLATE_BLUE                 0xFF6A5ACD
#define  C_MEDIUM_SLATE_BLUE          0xFF7B68EE
#define  C_MEDIUM_PURPLE              0xFF9370DB
#define  C_DARK_MAGENTA               0xFF8B008B
#define  C_DARK_VIOLET                0xFF9400D3
#define  C_DARK_ORCHID                0xFF9932CC
#define  C_MEDIUM_ORCHID              0xFFBA55D3
#define  C_PURPLE                     0xFF800080
#define  C_THISTLE                    0xFFD8BFD8
#define  C_PLUM                       0xFFDDA0DD
#define  C_VIOLET                     0xFFEE82EE
#define  C_MAGENTA                    0xFFFF00FF
#define  C_ORCHID                     0xFFDA70D6
#define  C_MEDIUM_VIOLET_RED          0xFFC71585
#define  C_PALE_VIOLET_RED            0xFFDB7093
#define  C_DEEP_PINK                  0xFFFF1493
#define  C_HOT_PINK                   0xFFFF69B4
#define  C_LIGHT_PINK                 0xFFFFB6C1
#define  C_PINK                       0xFFFFC0CB
#define  C_ANTIQUE_WHITE              0xFFFAEBD7
#define  C_BEIGE                      0xFFF5F5DC
#define  C_BISQUE                     0xFFFFE4C4
#define  C_BLANCHED_ALMOND            0xFFFFEBCD
#define  C_WHEAT                      0xFFF5DEB3
#define  C_CORN_SILK                  0xFFFFF8DC
#define  C_LEMON_CHIFFON              0xFFFFFACD
#define  C_LIGHT_GOLDEN_ROD_YELLOW    0xFFFAFAD2
#define  C_LIGHT_YELLOW               0xFFFFFFE0
#define  C_SADDLE_BROWN               0xFF8B4513
#define  C_SIENNA                     0xFFA0522D
#define  C_CHOCOLATE                  0xFFD2691E
#define  C_PERU                       0xFFCD853F
#define  C_SANDY_BROWN                0xFFF4A460
#define  C_BURLY_WOOD                 0xFFDEB887
#define  C_TAN                        0xFFD2B48C
#define  C_ROSY_BROWN                 0xFFBC8F8F
#define  C_MOCCASIN                   0xFFFFE4B5
#define  C_NAVAJO_WHITE               0xFFFFDEAD
#define  C_PEACH_PUFF                 0xFFFFDAB9
#define  C_MISTY_ROSE                 0xFFFFE4E1
#define  C_LAVENDER_BLUSH             0xFFFFF0F5
#define  C_LINEN                      0xFFFAF0E6
#define  C_OLD_LACE                   0xFFFDF5E6
#define  C_PAPAYA_WHIP                0xFFFFEFD5
#define  C_SEA_SHELL                  0xFFFFF5EE
#define  C_MINT_CREAM                 0xFFF5FFFA
#define  C_SLATE_GRAY                 0xFF708090
#define  C_LIGHT_SLATE_GRAY           0xFF778899
#define  C_LIGHT_STEEL_BLUE           0xFFB0C4DE
#define  C_LAVENDER                   0xFFE6E6FA
#define  C_FLORAL_WHITE               0xFFFFFAF0
#define  C_ALICE_BLUE                 0xFFF0F8FF
#define  C_GHOST_WHITE                0xFFF8F8FF
#define  C_HONEYDEW                   0xFFF0FFF0
#define  C_IVORY                      0xFFFFFFF0
#define  C_AZURE                      0xFFF0FFFF
#define  C_SNOW                       0xFFFFFAFA
#define  C_BLACK                      0xFF000000
#define  C_DIM_GRAY10                 0xFF101010
#define  C_DIM_GRAY20                 0xFF202020
#define  C_DIM_GRAY30                 0xFF303030
#define  C_DIM_GRAY                   0xFF696969
#define  C_GRAY                       0xFF808080
#define  C_DARK_GRAY                  0xFFA9A9A9
#define  C_SILVER                     0xFFC0C0C0
#define  C_LIGHT_GRAY                 0xFFD3D3D3
#define  C_GAINSBORO                  0xFFDCDCDC
#define  C_WHITE_SMOKE                0xFFF5F5F5
#define  C_WHITE                      0xFFFFFFFF
#endif




#endif
