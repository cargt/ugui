/* -------------------------------------------------------------------------------- */
/* -- µGUI - Generic GUI module (C)Achim Döbler, 2015                            -- */
/* -------------------------------------------------------------------------------- */
// µGUI is a generic GUI module for embedded systems.
// This is a free software that is open for education, research and commercial
// developments under license policy of following terms.
//
//  Copyright (C) 2015, Achim Döbler, all rights reserved.
//  URL: http://www.embeddedlightning.com/
//
// * The µGUI module is a free software and there is NO WARRANTY.
// * No restriction on use. You can use, modify and redistribute it for
//   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
// * Redistributions of source code must retain the above copyright notice.
//
/* -------------------------------------------------------------------------------- */
#ifndef __UGUI_TYPES_FONT_H
#define __UGUI_TYPES_FONT_H

#include <stdint.h>
#include "ugui_types.h"

typedef enum
{
	FONT_TYPE_1BPP,
	FONT_TYPE_8BPP,
	FONT_TYPE_TTF
} FONT_TYPE;

typedef struct
{
	UG_S16 const x;
	UG_S16 const y;
	UG_S16 const w;
	UG_S16 const h;
}fnt_rect_type;

typedef struct
{
	UG_S8 offset_x;
	UG_S8 offset_y;
	UG_U8 advance;
	fnt_rect_type rect;
}fnt_char_info_type;

typedef struct
{
   unsigned char* p;
   FONT_TYPE font_type;
   UG_S16 char_width;
   UG_S16 char_height;
   UG_U16 start_char;
   UG_U16 end_char;
   UG_U8  *widths;

   fnt_char_info_type* (*lookup_char)(UG_U16 const c);
   UG_S8 (*lookup_kerning_offset)(UG_U16 c1, UG_U16 c2);
   UG_U16 image_w;
   UG_U16 image_h;
} UG_FONT;




#endif
